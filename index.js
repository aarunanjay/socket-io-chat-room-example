var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

io.on('connection', function(socket){
  
  console.log('connection from: ' + socket.id);
  
  socket.on('chat', function(chat){
  	
  	var room = chat.room;
  	var message = chat.message;

  	console.log('chat received from client: ' + message + ' from room: ' + room);
    
    io.to(room).emit('chat-success', message); // send success message back to client
  });

  socket.on('create-or-join-room', function(room){
    console.log('create-or-join-room: ' + room);
    socket.join(room); // join a room

    // TODO : Validate if room can be joined..
  });
});