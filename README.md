# README #

* Make sure node is installed
* Run **node index.js**
* You should see **listening on *:3000**
* On the browser, go to **localhost:3000/**
* [Watch a demo](https://drive.google.com/open?id=0B90T7JKlkLHqdXgwWmN6Y0dZeXM&authuser=0)